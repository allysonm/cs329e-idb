from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def index():
	return render_template('static.html')

@app.route('/books/')
def books():
	return render_template('all_titles.html')

@app.route('/authors/')
def authors():
	return render_template('authors.html')

@app.route('/publishers/')
def publishers():
	return render_template('publishers.html')

@app.route('/about/')
def about():
	return render_template('about.html')

@app.route('/book1/')
def book1():
	return render_template('book1.html')

@app.route('/book2/')
def book2():
	return render_template('book2.html')

@app.route('/book3/')
def book3():
	return render_template('book3.html')

@app.route('/author1/')
def author1():
	return render_template('author1.html')

@app.route('/author2/')
def author2():
	return render_template('author2.html')

@app.route('/author3/')
def author3():
	return render_template('author3.html')

@app.route('/publisher1/')
def publisher1():
	return render_template('publisher1.html')

@app.route('/publisher2/')
def publisher2():
	return render_template('publisher2.html')

@app.route('/publisher3/')
def publisher3():
	return render_template('publisher3.html')

if __name__ == "__main__":
	app.run()